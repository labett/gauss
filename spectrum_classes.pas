unit spectrum_classes;

interface

uses
  Series, Chart, ComObj, CheckLst, SysUtils, Classes, Controls, StdCtrls,
  Dialogs,  iniFiles;

type
  TSValue = class
    x, val : Extended;
  end;

  IGraph = interface

  end;

  TSValueStorage = class
    private
      data : array of TSValue;
    function getCount: Integer;
    public
      constructor Create;
      function Add( val : TSValue) : Integer;
      function AddValues( val, x : Extended ) : Integer;
      function Delete( ix : Integer ) : Integer;
      function Get( ix : Integer ) : TSValue;
      procedure Clear;

      property Count : Integer read getCount;
  end;

  TSGraph = class
    private
      fStorage : TSValueStorage;
      fCaption: String;
      fExcelFileName: String;
    public
      constructor Create;

      procedure DrawToSeries( chrt : TChart; serNumber : Integer; isSelected : Boolean; isHide : Boolean ); virtual;
      function FillFromExcel( filename : String ) : Integer;
      function FillFromCSV( filename : String ) : Integer;

      function GetChild : TSGraph;
      function GetCopy : TSGraph;
      procedure CopyFrom( g : TSGraph );
      procedure Sub( g : TSGraph );
      function GetRMS : Extended;

      procedure RecalcuatePoints; virtual;

      property Caption : String read fCaption write fCaption;
      property ExcelFileName : String read fExcelFileName write fExcelFileName;
      property Storage : TSValueStorage read fStorage write fStorage;
  end;

  TSZeroLevel = class( TSGraph )
  private
    fZeroLevel: Extended;
    procedure setZeroLevel(const Value: Extended);
    public
      property ZeroLevel : Extended read fZeroLevel write setZeroLevel;
  end;

  TSGauss = class( TSGraph )
  private
    fScale: Extended;
    fPosition: Extended;
    fFat: Extended;
    public
      procedure RecalcuatePoints; override;
      procedure DrawToSeries( chrt : TChart; serNumber : Integer; isSelected : Boolean; isHide : Boolean ); override;

      property Fat : Extended read fFat write fFat;
      property Scale : Extended read fScale write fScale;
      property Position : Extended read fPosition write fPosition;
  end;

  TSGraphList = class ( TCheckListBox )
    private
      fGraphs : array of TSGauss;
      fZeroLevel : TSZeroLevel;
      fMain, fDif : TSGraph;
      fChart: TChart;
      fIsSaved: Boolean;
    fRMSEPrev: Extended;
    fRMSE: Extended;
      function getZeroLevel: Extended;
      procedure setZeroLevel(const Value: Extended);
      function getPosition: Extended;
      function getRmsWidth: Extended;
      function getScale: Extended;
      procedure setPosition(const Value: Extended);
      procedure setRmsWidth(const Value: Extended);
      procedure setScale(const Value: Extended);
    procedure setRMSE(const Value: Extended);
    public
      procedure AddMain(mg: TSGraph);
      procedure AddGauss( g : TSGauss );
      procedure DeleteGauss;
      procedure DrawGraphs;

      property RmsWidth : Extended read getRmsWidth write setRmsWidth;
      property Scale : Extended read getScale write setScale;
      property Position : Extended read getPosition write setPosition;
      property IsSaved : Boolean read fIsSaved;

      property RMSE : Extended read fRMSE write setRMSE;
      property RMSEPrev : Extended read fRMSEPrev;

      function curGauss : TSGauss;
      function getMain : TSGraph;

      procedure SaveToIni( iniFileName : String );
      function LoadFromIni( iniFileName : String ) : Boolean;
      procedure ExportToExcel;


    published
      property Chart : TChart read fChart write fChart;
      property ZeroLevel : Extended read getZeroLevel write setZeroLevel;
  end;

procedure Register;

implementation


procedure Register;
begin
  RegisterComponents('Boba', [TSGraphList]);
end;

{ TSGraph }

procedure TSGraph.CopyFrom(g: TSGraph);
var
  i : Integer;
  v : TSValue;
begin
  Storage.Clear;

  for i := 0 to g.Storage.getCount - 1 do begin
    v := TSValue.Create;
    v.x := g.Storage.Get(i).x;
    v.val := g.Storage.Get(i).val;
    Storage.Add( v );
  end;

  Caption := g.Caption;
end;

constructor TSGraph.Create;
begin
  storage := TSValueStorage.Create;
end;

procedure TSGraph.Sub(g: TSGraph);
var
  i : Integer;
begin
  if( g.Storage.getCount <> Storage.getCount ) then exit;

  for i := 0 to Storage.getCount - 1 do begin
    Storage.Get(i).val := Storage.Get(i).val - g.Storage.Get(i).val;
  end;
end;

procedure TSGraph.DrawToSeries(chrt: TChart; serNumber: Integer;
  isSelected: Boolean; isHide : Boolean);
var
  i : Integer;
  penWidth : Integer;
begin
  //if isHide then exit;
  chrt.series[ serNumber ].Clear;
  chrt.series[ serNumber ].Title := Caption;
  if isHide then  exit;

  if IsSelected then penWidth := 3 else penWidth := 2;
  if chrt.series[ serNumber ] is TLineSeries then begin
    (( chrt.series[ serNumber ] ) as TLineSeries ).LinePen.Width := penWidth;
  end;
  if chrt.series[ serNumber ] is TPointSeries then begin
    (( chrt.series[ serNumber ] ) as TPointSeries ).Pointer.ChangeHorizSize(penWidth);
    (( chrt.series[ serNumber ] ) as TPointSeries ).Pointer.ChangeVertSize(penWidth);
  end;


  for i := 0 to storage.Count - 1 do begin
    chrt.Series[ serNumber ].AddXY( storage.get(i).x, storage.get(i).val );
  end;
end;

function TSGraph.FillFromCSV( filename : String ) : Integer;
var
  s, tmp : TStringList;
  i : Integer;
  x, y : Double;
  d : Char;
begin

  try
    s := TStringList.Create;

    tmp := TStringList.Create;

    s.LoadFromFile( filename );

    for i := 1 to s.Count - 1  do begin
      tmp.Delimiter := ',';
      tmp.DelimitedText := s.Strings[i];
      d := DecimalSeparator;
      DecimalSeparator := '.';
      x := StrToFloat(tmp[0]);
      y := StrToFloat(tmp[1]);
      DecimalSeparator := d;
      storage.AddValues( y, x );
    end;
  except
    on E : Exception do begin
      ShowMessage(E.ClassName+' error raised, with message : '+E.Message);
      result := 0;
    end;
  end;
end;

function TSGraph.FillFromExcel(filename: String): Integer;
var
  vexcel : Variant;
  x, y : double;
  IsClose : Boolean;
  currow : Integer;

  procedure ReadNextValues;
  begin
    try
      x :=  VExcel.ActiveSheet.Cells.Item[curRow, 1].Value;
      y :=  VExcel.ActiveSheet.Cells.Item[curRow, 2].Value;
      inc( currow );
      if( (x = 0) and (y=0) ) then IsClose := true;
    except
      IsClose:= true;
    end;
  end;

begin
  fExcelFileName := filename;

    VExcel := CreateOleObject('Excel.Application');

    VExcel.Visible := True;
    //��������� Excel �� ������ �����
    VExcel.WindowState := -4137;
    //�� ���������� ��������������� ���������
    VExcel.DisplayAlerts := False;
    //��������� ������� �����
    try
      VExcel.WorkBooks.Open( fExcelFileName );
    except
      //ShowMessage('�� �������� ������� ���� Excel');
      result := -1;
      exit;
    end;
    //���������� �� ������ ����
    VExcel.WorkSheets[1].Activate;

    IsClose:= false;
    currow := 1;

    while not IsClose do begin
      ReadNextValues;
      if( not IsClose ) then begin
        storage.AddValues( y, x );
      end;
    end;

    result := storage.Count;
    VExcel.Quit;
end;

function TSGraph.GetChild: TSGraph;
var
  i : Integer;
  v : TSValue;
begin
  result := TSGraph.Create;

  for i := 0 to fStorage.getCount - 1 do begin
    v := TSValue.Create;
    v.x := Storage.Get(i).x;
    v.val := 0;
    result.Storage.Add( v );
  end;

  result.Caption := Caption;
end;

function TSGraph.GetCopy: TSGraph;
var
  i : Integer;
  v : TSValue;
begin
  result := TSGraph.Create;

  for i := 0 to fStorage.getCount - 1 do begin
    v := TSValue.Create;
    v.x := Storage.Get(i).x;
    v.val := Storage.Get(i).val;
    result.Storage.Add( v );
  end;

  result.Caption := Caption;
end;

procedure TSGraph.RecalcuatePoints;
begin

end;

procedure TSGraphList.ExportToExcel;
var
  vexcel : Variant;
  i : Integer;
  j, r : Integer;
  curCol : Integer;
begin
    VExcel := CreateOleObject('Excel.Application');

    VExcel.Visible := True;
    VExcel.WindowState := -4137;
    VExcel.DisplayAlerts := False;
    VExcel.Workbooks.Add;
    //VExcel.WorkSheets.Add;
    VExcel.WorkSheets[1].Activate;
    curCol := 1;

      VExcel.ActiveSheet.Cells.Item[1, curCol] := fMain.fCaption;
      VExcel.ActiveSheet.Cells.Item[1, curCol+1] := 'X';
      VExcel.ActiveSheet.Cells.Item[1, curCol+2] := 'Y';
      for j := 0 to fMain.Storage.Count - 1 do begin
        VExcel.ActiveSheet.Cells.Item[j+2, curCol + 1] := fMain.Storage.Get(j).x;
        VExcel.ActiveSheet.Cells.Item[j+2, curCol + 2] := fMain.Storage.Get(j).val;
      end;
      curCol := curCol + 3;

      VExcel.ActiveSheet.Cells.Item[1, curCol] := fDif.fCaption;
      VExcel.ActiveSheet.Cells.Item[1, curCol+1] := 'X';
      VExcel.ActiveSheet.Cells.Item[1, curCol+2] := 'Y';
      for j := 0 to fDif.Storage.Count - 1 do begin
        VExcel.ActiveSheet.Cells.Item[j+2, curCol + 1] := fDif.Storage.Get(j).x;
        VExcel.ActiveSheet.Cells.Item[j+2, curCol + 2] := fDif.Storage.Get(j).val;
      end;
      curCol := curCol + 3;


    for i := 0 to Length( fGraphs ) - 1 do begin
      VExcel.ActiveSheet.Cells.Item[1, curCol] := fGraphs[i].fCaption;
      VExcel.ActiveSheet.Cells.Item[1, curCol+1] := 'X';
      VExcel.ActiveSheet.Cells.Item[1, curCol+2] := 'Y';
      for j := 0 to fGraphs[i].Storage.Count - 1 do begin
        VExcel.ActiveSheet.Cells.Item[j+2, curCol + 1] := fGraphs[i].Storage.Get(j).x;
        VExcel.ActiveSheet.Cells.Item[j+2, curCol + 2] := fGraphs[i].Storage.Get(j).val;
      end;
      curCol := curCol + 3;
    end;
end;

function TSGraph.GetRMS : Extended;
var
  i : Integer;
  v : TSValue;
  curV : Extended;
begin
  result := 0;
  for i := 0 to fStorage.getCount - 1 do begin
    curV := Storage.Get(i).val;
    result :=   result + curV * curV;
  end;
  result := sqrt( result / fStorage.getCount );
end;

{ TSValueStorage }

function TSValueStorage.Add(val: TSValue): Integer;
begin
  SetLength( data, Length( data ) + 1 );
  data[ Length(data) - 1 ] := val;
  result := Length( data );
end;

function TSValueStorage.AddValues(val, x: Extended): Integer;
var
  tsval : TSValue;
begin
  tsval := TSValue.Create;

  tsval.x := x;
  tsval.val := val;

  result := Add( tsval );
end;

procedure TSValueStorage.Clear;
begin
  while( Count > 0 ) do begin
    Delete( Count - 1 );
  end;
end;

constructor TSValueStorage.Create;
begin
  SetLength( data, 0 );
end;

function TSValueStorage.Delete(ix: Integer): Integer;
begin
  if( (ix < 0) or ( ix >= Length(data) ) ) then begin
    result :=  0;
    exit;
  end;
  data[ ix ].Free;
  data[ ix ] := data[ Count - 1 ];
  SetLength( data, Count - 1 );
  result := Count - 1;
end;

function TSValueStorage.Get(ix: Integer): TSValue;
begin
  if( (ix < 0) or ( ix >= Length(data) ) ) then begin
    result :=  nil;
    exit;
  end;
  result := data[ ix ];
end;

function TSValueStorage.getCount: Integer;
begin
  result := Length( data );
end;

{ TSGraphList }

procedure TSGraphList.AddGauss(g: TSGauss);
begin
  SetLength( fGraphs, Length( fGraphs ) + 1 );
  fGraphs[ Length(fGraphs) - 1 ] := g;
//  Items.Add( );

  DrawGraphs;
end;

procedure TSGraphList.AddMain(mg: TSGraph);
var
  storedMg, storedDif : TSGraph;
begin
  SetLength( fGraphs, 0 );
  fMain := mg.GetCopy;
  fDif := mg.GetChild;
  fZeroLevel := TSZeroLevel.Create;
  fZeroLevel.CopyFrom( mg );
  fZeroLevel.ZeroLevel := 0;

  fDif.Caption := 'Difference';
  fZeroLevel.Caption := 'ZeroLevel';

  Items.Clear;
  Items.Add( fMain.Caption );
  Items.Add( fDif.Caption );
  Items.Add( fZeroLevel.Caption );
  Checked[0] := true;
  Checked[1] := true;
  Checked[2] := true;

  chart.AddSeries( TPointSeries.Create( self ) );
  chart.Series[0].Title := fMain.Caption;
  chart.AddSeries( TPointSeries.Create( self ) );
  chart.Series[1].Title := fDif.Caption;
  chart.AddSeries( TLineSeries.Create( self ) );
  chart.Series[2].Title := fZeroLevel.Caption;

  DrawGraphs;
  fIsSaved := false;
end;

function TSGraphList.curGauss: TSGauss;
var
  i : Integer;
begin
  result := nil;
  for i := 3 to Items.Count - 1 do begin
    if( Selected[i] ) then begin
      result := fGraphs[i-3];
      exit;
    end;
  end;
end;

procedure TSGraphList.DeleteGauss;
var
  i,j : Integer;
begin
 for i := 3 to Items.Count - 1 do begin
   if Selected[i] then begin
     fGraphs[i-3].Free;
     for j := i to Items.Count - 2 do begin
       fGraphs[j-3] := fGraphs[j-2];
     end;
     SetLength( fGraphs, Length( fGraphs ) - 1 );
     Items.Delete( i);
     Chart.RemoveSeries( Chart.Series[i] );
     fIsSaved := false;
     exit;
   end;
 end;
end;

procedure TSGraphList.DrawGraphs;
var
   i : integer;

   procedure AddSeries;
   var
     s : TPointSeries;
   begin
     s := TPointSeries.Create( self );
     s.Title := fgraphs[i].Caption;
     Chart.AddSeries( s );
   end;
begin
  //while( Chart.SeriesCount < 3 + Length( fGraphs ) - 1  ) do AddSeries;

  if( fMain = nil ) then exit;
  fDif.CopyFrom( fMain );
  fDif.Caption := 'Difference';
  fDif.Sub( fZeroLevel );
  for i := 0 to Length( fGraphs ) - 1 do begin
    fGraphs[i].RecalcuatePoints;
    fDif.Sub( fGraphs[i] );
  end;

  RMSE := fDif.GetRMS;

  fMain.DrawToSeries( fChart, 0, Selected[0],  not Checked[0]  );
  fDif.DrawToSeries( fChart, 1, Selected[1],  not Checked[1]  );
  fZeroLevel.DrawToSeries( fChart, 2, Selected[2],  not Checked[2]  );

  for i := 0 to Length( fGraphs ) - 1 do begin
    //fGraphs[i].RecalcuatePoints;
    if Chart.SeriesCount < i + 1 + 3 then begin
      AddSeries;
    end;
    if( Items.Count < i + 1 + 3 ) then Items.Add( fGraphs[i].Caption) else Items[i+3] := fGraphs[i].Caption;
    fGraphs[i].DrawToSeries( fChart, i + 3, Selected[i+3],  not Checked[i + 3]  );
  end;
end;


function TSGraphList.getMain: TSGraph;
begin
  result := fMain;
end;

function TSGraphList.getPosition: Extended;
var
  cg : TSGauss;
begin
  cg := curGauss;
  result := 0;
  if( cg = nil ) then exit;
  result := cg.Position;
end;

function TSGraphList.getRmsWidth: Extended;
var
  cg : TSGauss;
begin
  cg := curGauss;
  result := 0;
  if( cg = nil ) then exit;
  result := cg.Fat;
end;

function TSGraphList.getScale: Extended;
var
  cg : TSGauss;
begin
  cg := curGauss;
  result := 0;
  if( cg = nil ) then exit;
  result := cg.Scale;
end;

function TSGraphList.getZeroLevel: Extended;
begin
  result := 0;
  //if Length( fGraphs ) < 3 then exit;
  if( fZeroLevel <> nil ) then result := fZeroLevel.ZeroLevel;
end;

function TSGraphList.LoadFromIni(iniFileName: String): Boolean;
var
  ini : TIniFile;
  i : Integer;
  mg : TSGraph;
  g : TSGauss;
  ver : Integer;
  v : TSValue;
  gCount : Integer;
begin
  result := false;
  ini := TIniFile.Create( iniFileName );

  if( ini.ReadString('Software', 'Name', 'xxx') <> 'Gauss2' ) then begin
    ShowMessage('Error: Unknown file type!' );
    exit;
  end;
  ver := ini.ReadInteger('Software', 'Version', 0);

  if( ver = 1 ) then begin
    mg := TSGraph.Create;
    mg.Caption := ini.ReadString('Source', 'Caption', 'Noname');
    for i := 0 to ini.ReadInteger('Source', 'PointCount', 0 ) - 1 do begin
      v := TSValue.Create;
      v.x := ini.ReadFloat('SourceValuesX', IntToStr(i), 0 );
      v.val := ini.ReadFloat('SourceValuesY', IntToStr(i), 0 );
      mg.Storage.Add( v );
    end;

    AddMain( mg );
    ZeroLevel := ini.ReadFloat('Source', 'ZeroLevel', 0 );

    gCount := ini.ReadInteger('Gauss', 'GaussCount', 0 );
    for i := 0 to gCount - 1  do begin
      g := TSGauss.Create;
      g.CopyFrom( mg );
      g.Position := ini.ReadFloat('GaussPosition', IntToStr(i), 0 );
      g.Fat :=      ini.ReadFloat('GaussRmsWidth', IntToStr(i), 0 );
      g.Scale :=    ini.ReadFloat('GaussScale',    IntToStr(i), 0 );
      AddGauss( g );
    end;

  end else begin
    ShowMessage('Unknown file version. Expected <1>, found <' + IntToStr(ver) + '>' );
    exit;
  end;

  fIsSaved := true;
  result := true;
end;

procedure TSGraphList.SaveToIni(iniFileName: String);
var
  ini : TIniFile;
  i : Integer;
begin
  if fMain = nil then exit;

  ini := TIniFile.Create( iniFileName );

  ini.WriteString('Software', 'Name', 'Gauss2');
  ini.WriteInteger('Software', 'Version', 1);

  ini.WriteString('Source', 'Caption', fMain.Caption);
  ini.WriteInteger('Source', 'PointCount', fMain.Storage.getCount );
  for i := 0 to fMain.Storage.getCount - 1 do begin
    ini.WriteFloat('SourceValuesX', IntToStr(i), fMain.Storage.Get(i).x );
    ini.WriteFloat('SourceValuesY', IntToStr(i), fMain.Storage.Get(i).val );
  end;
  ini.WriteFloat('Source', 'ZeroLevel', ZeroLevel );

  ini.WriteInteger('Gauss', 'GaussCount', Length(fGraphs) );
  for i := 0 to Length(fGraphs) - 1  do begin
    ini.WriteFloat('GaussPosition', IntToStr(i), fGraphs[i].Position );
    ini.WriteFloat('GaussRmsWidth', IntToStr(i), fGraphs[i].Fat );
    ini.WriteFloat('GaussScale', IntToStr(i), fGraphs[i].Scale );
  end;

  fIsSaved := true;
end;

procedure TSGraphList.setPosition(const Value: Extended);
var
  cg : TSGauss;
begin
  cg := curGauss;
  if( cg = nil ) then exit;
  cg.Position := Value;
  DrawGraphs;
  fIsSaved := false;
end;

procedure TSGraphList.setRmsWidth(const Value: Extended);
var
  cg : TSGauss;
begin
  cg := curGauss;
  if( cg = nil ) then exit;
  cg.Fat := Value;
  DrawGraphs;
  fIsSaved := false;
end;

procedure TSGraphList.setScale(const Value: Extended);
var
  cg : TSGauss;
begin
  cg := curGauss;
  if( cg = nil ) then exit;
  cg.Scale := Value;
  DrawGraphs;
  fIsSaved := false;
end;

procedure TSGraphList.setZeroLevel(const Value: Extended);
begin
  //if Length( fGraphs ) < 3 then exit;
  if( fZeroLevel <> nil ) then fZeroLevel.ZeroLevel := Value;

  DrawGraphs;
  fIsSaved := false;
end;

procedure TSGraphList.setRMSE(const Value: Extended);
begin
  fRMSEPrev := fRMSE;
  fRMSE := Value;
end;

{ TSZeroLevel }

procedure TSZeroLevel.setZeroLevel(const Value: Extended);
var
  i : Integer;
begin
  fZeroLevel := Value;

  for i := 0 to Storage.getCount - 1 do begin
    Storage.Get(i).val := fZeroLevel;
  end;
end;

{ TSGauss }

procedure TSGauss.DrawToSeries(chrt: TChart; serNumber: Integer;
  isSelected, isHide: Boolean);
var
  i : Integer;
  penWidth : Integer;
begin
  chrt.series[ serNumber ].Clear;
  chrt.series[ serNumber ].Title := Caption;
  if isHide then exit;
  if IsSelected then penWidth := 3 else penWidth := 2;
  if chrt.series[ serNumber ] is TLineSeries then begin
    (( chrt.series[ serNumber ] ) as TLineSeries ).LinePen.Width := penWidth;
  end;
  if chrt.series[ serNumber ] is TPointSeries then begin
    (( chrt.series[ serNumber ] ) as TPointSeries ).Pointer.ChangeHorizSize(penWidth);
    (( chrt.series[ serNumber ] ) as TPointSeries ).Pointer.ChangeVertSize(penWidth);
  end;


  for i := 0 to storage.Count - 1 do begin
    if( storage.get(i).val > 0.1 * fScale ) then begin
      chrt.Series[ serNumber ].AddXY( storage.get(i).x, storage.get(i).val );
    end;
  end;
end;

procedure TSGauss.RecalcuatePoints;
var
  i : Integer;
  x : Extended;
begin
  inherited;

  for i := 0 to Storage.getCount - 1 do begin
    x := Storage.Get(i).x;
    if( fFat <> 0 ) then begin
      Storage.Get(i).val := exp( -(x - fPosition) * (x - fPosition) / ( 2 * fFat * fFat ) ) * fScale;
    end else Storage.Get(i).val := 0;
  end;
  if fScale > 10 then begin
    fCaption := Format( '%.2f-%.2f(%.0f)', [fPosition, fFat, fScale]);
  end else begin
    fCaption := Format( '%.2f-%.2f(%.2f)', [fPosition, fFat, fScale]);
  end;

end;

end.
