object frmMain: TfrmMain
  Left = 203
  Top = 138
  Width = 1170
  Height = 599
  Caption = 'Gauss v'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 966
    Top = 26
    Height = 534
    Align = alRight
  end
  object Panel1: TPanel
    Left = 969
    Top = 26
    Width = 185
    Height = 534
    Align = alRight
    TabOrder = 0
    object glMain: TSGraphList
      Left = 1
      Top = 1
      Width = 183
      Height = 248
      OnClickCheck = glMainClick
      Align = alTop
      ItemHeight = 13
      TabOrder = 0
      OnClick = glMainClick
      Chart = chrtMain
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 488
      Width = 183
      Height = 45
      Align = alBottom
      Caption = 'Zero level'
      TabOrder = 1
      object edtZero: TSpinEdit
        Left = 8
        Top = 16
        Width = 161
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
        OnChange = edtZeroChange
      end
    end
    object GroupBox3: TGroupBox
      Left = 1
      Top = 249
      Width = 183
      Height = 208
      Align = alTop
      Caption = 'RMS Error'
      TabOrder = 2
      object Label4: TLabel
        Left = 8
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Full Range'
      end
      object lblFullRMSE: TLabel
        Left = 8
        Top = 32
        Width = 107
        Height = 25
        Caption = 'lblFullRMSE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -20
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 8
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Previous value'
      end
      object lblPrevRMSE: TLabel
        Left = 8
        Top = 72
        Width = 107
        Height = 25
        Caption = 'lblFullRMSE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 16744448
        Font.Height = -20
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 26
    Width = 966
    Height = 534
    Align = alClient
    TabOrder = 1
    object chrtMain: TChart
      Left = 1
      Top = 1
      Width = 964
      Height = 383
      BackWall.Brush.Color = clWhite
      BackWall.Brush.Style = bsClear
      Gradient.EndColor = clWhite
      Title.Text.Strings = (
        'TChart')
      Title.Visible = False
      BottomAxis.ExactDateTime = False
      BottomAxis.Increment = 0.100000000000000000
      BottomAxis.Title.Caption = 'hv, eV'
      LeftAxis.Title.Caption = 'L, arb.un.'
      Legend.ColorWidth = 15
      Legend.LegendStyle = lsSeries
      Legend.TextStyle = ltsPlain
      Legend.TopPos = 6
      View3D = False
      Align = alClient
      Color = clWhite
      TabOrder = 0
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 384
      Width = 964
      Height = 149
      Align = alBottom
      Anchors = [akTop, akRight]
      Caption = 'Gauss properties'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        964
        149)
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 27
        Height = 13
        Caption = 'Scale'
      end
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 88
        Height = 13
        Caption = 'RMS width (sigma)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 8
        Top = 72
        Width = 37
        Height = 13
        Caption = 'Position'
      end
      object Image1: TImage
        Left = 600
        Top = 8
        Width = 361
        Height = 73
        Anchors = [akTop, akRight]
        Center = True
        Picture.Data = {
          0954474946496D6167654007000047494638396154013A00B30000FFFFFF0000
          00AAAAAA444444EEEEEE101010CCCCCCBABABA666666323232DCDCDC76767698
          989822222254545488888821F90401000000002C0000000054013A000004FE10
          C849ABBD38EBCDBBFF60288E64699E686A190B322C6A2CCF746DDF783E1BCF34
          24BAA070482C1A8F2104C51060209FD0A8744A9D14609380A3CAED7ABFE04D42
          991D84CFE8B4DAA808F4D6F0F84CA0900B1984CDA290B7FBFF20076F340C0301
          0D08890E0306270F8D1B8F285B194C90809899160494364C02140C0175240405
          9700060D15A6A82406641709079AB5B608B4370F017D59B1290B403603A4150E
          A0B6C998C23703AB14045A3209583507BF120FC8A9CADD7002D83405D80701DB
          2901B9360115024E13D5DEF260C7160A0C0F4A0F0C9D216DEF3ECC4C20B0E0C1
          837804F6317877CF01A46B8612356AE88AA0412CEE94B87BE0A0D78404FE970C
          24307871D0BC935C40B27A3760000101BC46882AC6A9E3840309FAB4F828215A
          1D183B273C600600E82F9C3A95381907C9413C090E00160840B5EA399458A314
          2826012035095C413870531255AB9BEC00081008A0CE81460D4CBEB80977D059
          09E50E2888B66D6E050426B30AF612135A16751314285ECC786F85026C2F3820
          2A4A0293915CDB784C5741F384C9A1D296A350E02A807C835313F64861746707
          B063CB7660B24DE0C726113C539B80AABA059199B0064E3AF733E2600B5340AD
          BAF994AD18829D10E58A421BC457DA166B69E5DCD09EDD1373B68265243CA2F0
          6E3B5F4FA4812B52637A22FEE0A0C086009704D8078068020332A311FEF08E03
          64BC11E03BF84DA05F169734E04460F5B027611111E2358A4858A8D701641B44
          05964A00D824814B5D99F1401F1EBE55E269283204A26B128C226005204E68A3
          0ED75887C04204EC88870808F4360E6BD0EC78502F0A2874E240030810D231EA
          10D0E4253DE2B3402FFF5150104013EC76E397377809A605073C35E69929E082
          A605C4ACE9260A52BE69999972D6E981001A7ED98F9D7CDE1916983FF679A602
          06146AE8A1860A7AA301F830C0D57C392067C76409546AE9A5090CA04E559C76
          EAE9A7A0862AEAA8A4966AEAA9A36AC2400163E4D300891CE9A0C0545485A328
          123CF8809EA20424E05E050CCCD244100A1C309569B71EF10B135CFE0A4A4003
          BB4AB000557FDE401591C91A915D167BF639001F1844B35F1097652B457C6524
          5B0E9D5DDA6AC32EEC9A8B846DC98E05290509206B436FF7CA8B010B2EC46BC2
          1ED8D6D9408219E4340455FE7A90EB88D19A6049B6070B9C270DE544AC82020D
          F43BD8B2C3C6300B156BB544A47ED53D31D67821AC95E90BD82A10644B8D1850
          EDB40217D2D200CD76504ECF20E8DC12D0686C1B63B7255418458E00B06AC154
          1E1BC10455D4A47C812A351A1059576EF4A46900D1F28B817E0E14435B089F90
          40B6D9177F816E8C5B97A0CD04560B11678C6989A7DC14A274CAB3063045C631
          C2D2764D77AD9B307C01CE9D8979C1357DB471C93E1E306E9DE3FE71D04BC18E
          571BC2E9B8EE6809C502B930312ED79857500EAA559D0E82CC0773DA00B6E588
          39ED280A82FD74C814C014EDB471E3C4C158A7C458C7E0712F1E40F01AAB4130
          784521D3CB010DD0415D2F22917450DB3930834000D88CE56E6788966FBECD27
          3454D5AEA6B00C5685D110DE13B5167C6FE6D47FE6DB01031503C08E2805E05E
          2AE847237DA9616212D0CD3D0C60AC7744E312033009AD3865407265417E4D33
          871A56E68AEFA56E7301709D5A4278818341CA10444952028836B64A41A6002C
          6493EE12339418A661644B580481CCF60BBFA0A1769DB9D61A0ED62CAA080C6F
          66FA5EB7E29738B0C9660115D4000A0B7681F825E089514C83FED2FC9329C4C8
          AF7A6AB01FB00E418562752800F1800906C94495EA10D102BE63E31A45F08002
          D4070101A4626BDAA88CB959460202880B03AEF1AB3658676F61988A2B224205
          28EA817745519C066ED744228DE5367D3BC11E86C50BFDA0518F00C864324207
          0F40D6616E2A32E4041070442A4C6D775904C0EA5827490ED40803BDF1880739
          F03D11C26437C5502458FC43C6A4818D14B8EBD50C33208A0FDA217B2429C85F
          9A95005214620DBFACC0D4409998F379B350D5C280FB12B7B55D24AF2779005E
          FDC037A7E4E48D2D4CF8200140C928F1A863901B886706E61987095A459BACA9
          12E5B0B9BC0A4CCB9944589706C612968C6140010A83C99EF3E2F70E0440228E
          6D610B132F00516E2E41831FD8A83D14D6B01BB4C14BB362E7146E174E5120CB
          10441209294C411429995002CC90A8B4CE31ADEAC834043F0B414F5750CD92E6
          602C90D01A556219045FE9E72A9C00A93D5EB58444780426A523864B4F03109A
          02E06E14F801F6AC2A825ED52D0362A51B598D9A8307BCCA29DB9C8229EA2012
          3B2E6001F5299B1E5AB2880554EB003BBB52365E151891B4A45A0FE0EB0BC289
          83C40EA0AF8C65AB2E0A2A575428004FDAF0A864371B829B72F6B3F2C02362B2
          09DAD226636A5C3A58644DCBDA35182B1E86885A6B677BC3993AE057B4CDAD1D
          14B0881F0850B7C0A5420400003B}
      end
      object edtScaleStep: TEdit
        Left = 240
        Top = 16
        Width = 57
        Height = 21
        TabOrder = 0
        Text = '50'
        OnChange = edtScaleStepChange
      end
      object BitBtn1: TBitBtn
        Left = 8
        Top = 96
        Width = 137
        Height = 25
        Caption = 'Add New Gaussian'
        TabOrder = 1
        OnClick = BitBtn1Click
        Kind = bkYes
      end
      object edtRmsStep: TEdit
        Left = 240
        Top = 40
        Width = 57
        Height = 21
        TabOrder = 2
        Text = '0,005'
        OnChange = edtScaleStepChange
      end
      object edtPosStep: TEdit
        Left = 240
        Top = 64
        Width = 57
        Height = 21
        TabOrder = 3
        Text = '0,005'
        OnChange = edtScaleStepChange
      end
      object edtScale: TRxSpinEdit
        Left = 104
        Top = 16
        Width = 121
        Height = 21
        Decimal = 4
        Increment = 50.000000000000000000
        ValueType = vtFloat
        TabOrder = 4
        OnChange = edtScaleChange
      end
      object edtRms: TRxSpinEdit
        Left = 104
        Top = 40
        Width = 121
        Height = 21
        Decimal = 3
        Increment = 0.005000000000000000
        ValueType = vtFloat
        TabOrder = 5
        OnChange = edtRmsChange
      end
      object edtPos: TRxSpinEdit
        Left = 104
        Top = 64
        Width = 121
        Height = 21
        Decimal = 3
        Increment = 0.005000000000000000
        ValueType = vtFloat
        TabOrder = 6
        OnChange = edtPosChange
      end
      object BitBtn2: TBitBtn
        Left = 520
        Top = 96
        Width = 137
        Height = 25
        Caption = 'Delete spectral line'
        TabOrder = 7
        OnClick = BitBtn2Click
        Kind = bkCancel
      end
      object cbxZero: TCheckBox
        Left = 472
        Top = 12
        Width = 73
        Height = 17
        Anchors = [akLeft, akBottom]
        Caption = 'minX = 0'
        TabOrder = 8
        OnClick = cbxZeroClick
      end
      object edtName: TEdit
        Left = 320
        Top = 16
        Width = 121
        Height = 21
        TabOrder = 9
        Text = 'edtName'
        OnChange = edtNameChange
      end
    end
  end
  object ActionMainMenuBar1: TActionMainMenuBar
    Left = 0
    Top = 0
    Width = 1154
    Height = 26
    UseSystemFont = False
    ActionManager = ActionManager1
    Caption = 'ActionMainMenuBar1'
    ColorMap.HighlightColor = clWhite
    ColorMap.BtnSelectedColor = clBtnFace
    ColorMap.UnusedColor = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Spacing = 0
  end
  object ActionManager1: TActionManager
    ActionBars = <
      item
        Items = <
          item
            Items = <
              item
                Action = actSaveProject
                Caption = '&Save project'
              end
              item
                Action = Action1
                Caption = '&Open project'
              end
              item
                Action = Action4
              end
              item
                Action = Action3
                Caption = '&Import Excel'
              end
              item
                Action = actImportCsv
                Caption = 'I&mport CSV'
              end
              item
                Action = actToExcel
                Caption = '&Export to Excel'
              end
              item
                Action = actCopyToClipboard
                Caption = 'E&xport to Clipboard'
              end
              item
                Action = Action2
              end>
            Caption = 'F&ile'
          end
          item
            Action = Action5
            Caption = '&Help'
          end>
        ActionBar = ActionMainMenuBar1
      end>
    Left = 88
    Top = 216
    StyleName = 'XP Style'
    object actSaveProject: TAction
      Category = 'File'
      Caption = 'Save project'
      OnExecute = actSaveProjectExecute
    end
    object Action1: TAction
      Category = 'File'
      Caption = 'Open project'
      OnExecute = Action1Execute
    end
    object Action4: TAction
      Category = 'File'
      Caption = '-'
    end
    object Action3: TAction
      Category = 'File'
      Caption = 'Import Excel'
      OnExecute = Action3Execute
    end
    object actImportCsv: TAction
      Category = 'File'
      Caption = 'Import CSV'
      OnExecute = actImportCsvExecute
    end
    object actToExcel: TAction
      Category = 'File'
      Caption = 'Export to Excel'
      OnExecute = actToExcelExecute
    end
    object actCopyToClipboard: TAction
      Category = 'File'
      Caption = 'Export to Clipboard'
      OnExecute = actCopyToClipboardExecute
    end
    object Action2: TAction
      Category = 'File'
      Caption = 'Exit'
      OnExecute = Action2Execute
    end
    object Action5: TAction
      Category = 'Help'
      Caption = 'Help'
      OnExecute = Action5Execute
    end
  end
  object dlgOpenExcel: TOpenDialog
    Filter = 'Excel|*.xls'
    Left = 745
    Top = 98
  end
  object dlgSaveProj: TSaveDialog
    DefaultExt = '*.g2p'
    Filter = 'Gauss2 files( *.g2p)|*.g2p|All files|*.*'
    Left = 745
    Top = 162
  end
  object dlgOpenProj: TOpenDialog
    Filter = 'Gauss2 files( *.g2p)|*.g2p|All files|*.*'
    Left = 745
    Top = 210
  end
end
