unit gauss_f;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, gauss_classes,
  Spin, gGaussList, RXSpin, Buttons, ActnList, XPStyleActnCtrls, ActnMan, comobj,
  ComCtrls, spectrum_classes;

type
  TForm1 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Button1: TButton;
    GroupBox1: TGroupBox;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Chart1: TChart;
    Series1: TLineSeries;
    Series16: TLineSeries;
    Series15: TLineSeries;
    Series2: TLineSeries;
    Series3: TLineSeries;
    Series4: TLineSeries;
    Series5: TLineSeries;
    Series6: TLineSeries;
    Series7: TLineSeries;
    Series8: TLineSeries;
    Series9: TLineSeries;
    Series10: TLineSeries;
    Series11: TLineSeries;
    Series12: TLineSeries;
    Series13: TLineSeries;
    Series14: TLineSeries;
    Series17: TLineSeries;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    btnSetMain: TButton;
    Button5: TButton;
    Button2: TButton;
    edtDy: TSpinEdit;
    cbxShowCurGauss: TCheckBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    edtdx_step: TEdit;
    edtScale_step: TEdit;
    edtFat_step: TEdit;
    Button3: TButton;
    Button4: TButton;
    edtBandCaption: TEdit;
    ActionManager1: TActionManager;
    actOpen: TAction;
    actGausesSave: TAction;
    actGausesLoad: TAction;
    dlgOpen: TOpenDialog;
    dlgSaveGauss: TSaveDialog;
    dlgOpenGauss: TOpenDialog;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    Chart2: TChart;
    LineSeries2: TLineSeries;
    LineSeries3: TLineSeries;
    LineSeries4: TLineSeries;
    LineSeries5: TLineSeries;
    LineSeries6: TLineSeries;
    LineSeries7: TLineSeries;
    LineSeries8: TLineSeries;
    LineSeries9: TLineSeries;
    LineSeries10: TLineSeries;
    LineSeries11: TLineSeries;
    LineSeries12: TLineSeries;
    LineSeries13: TLineSeries;
    LineSeries14: TLineSeries;
    LineSeries15: TLineSeries;
    LineSeries16: TLineSeries;
    LineSeries17: TLineSeries;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    LineSeries1: TPointSeries;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure lbGausesMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edtDxChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure edtScaleChange(Sender: TObject);
    procedure edtFatChange(Sender: TObject);
    procedure edtDyChange(Sender: TObject);
    procedure cbxShowCurGaussClick(Sender: TObject);
    procedure actOpenExecute(Sender: TObject);
    procedure actGausesLoadExecute(Sender: TObject);
    procedure actGausesSaveExecute(Sender: TObject);
    procedure edtdx_stepChange(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure btnSetMainClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
  private
    { Private declarations }
  public
    rand : TgGraph;
    gauss : TgGauss;
    source : TgGraph;

    spectrum : TSGraph;

    function CreateGauss : TgGauss;
  end;

var
  Form1: TForm1;

implementation


{$R *.dfm}
function TForm1.CreateGauss  : TgGauss;
var
  tmp : TgGauss;
begin
  tmp := TgGauss.Create( 0, 0, 0 );
  tmp.CopyGraph( source );

  tmp.Caption := edtBandCaption.Text;
  tmp.IsSimpleGraph := false;


  lbGauses.AddGraph( tmp  );
//  Chart1.Series.Add()/
  result := tmp;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
 { rand := TgGraph.Create;
  source := TgGraph.Create;

  gauss := TgGauss.Create;

  gauss.stepCount := 400;
  gauss.minX := 400;
  gauss.maxX := 800;

  gauss.DX := 600;
  gauss.Fat := 1000;
  gauss.Scale := 0;

  gauss.Caption := '�������� 1';

  gauss.FillGauss;

//  lbGauses.AddGauss( gauss );

  gauss := TgGauss.Create;

  gauss.stepCount := 400;
  gauss.minX := 400;
  gauss.maxX := 800;

  gauss.DX := 600;
  gauss.Fat := 1000;
  gauss.Scale := 10;

  gauss.Caption := '�������� 2';

  gauss.FillGauss;

//  lbGauses.AddGauss( gauss );
//  gauss.Draw( chart1, 0 );


  rand.stepCount := 400;
  rand.minX := 400;
  rand.maxX := 800;

  rand.FillRandom;

//  rand.Draw( chart1, 1 );

  rand.AddGraph( gauss );

  gauss.DX := 500;
  gauss.Scale := 5;
  gauss.Fat := 500;
  gauss.FillGauss;
  rand.AddGraph( gauss );

  rand.Draw( chart1, 0 );

  source.CopyGraph( rand );   }

  spectrum := TSGraph.Create;
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
//  rand.Free;
//  gauss.Free;
//  source.Free;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  i : Integer;
  rvis : Boolean;
begin
  if( glMain.Count < 1 ) then begin
    ShowMessage('������� ������ ������ ���� ��� ������� ����!');
    exit;
  end;

  for i := 0 to Chart1.SeriesCount - 1 do begin
    Chart1.Series[i].Clear;
  end;

  rvis := lbGauses.GetGraph(1).Visible;
  rand.CopyGraph( glMain.GetGraph(0) );
  rand.Visible := rvis;
  for i := 1 to glMain.Count - 1 do begin
    rand.AddGraph( glMain.GetGraph(i) );
  end;

  for i := 0 to glSubs.Items.Count - 1 do begin
    gauss := TgGauss( glSubs.GetGraph(i) );
    gauss.FillGauss;
    rand.SubGraph( gauss );
  end;

  for i := 0 to lbGauses.Items.Count - 1 do begin
    gauss :=  lbGauses.GetGraph(i) as TGGauss;
    gauss.FillGauss;
    if gauss.Visible then begin
      gauss.Draw( Chart1, i, lbGauses.Selected[i], gkCV );
    end;
  end;

{  chart1.SeriesList[0].Clear;
  rand.Draw( Chart1, 0 );
  chart1.SeriesList[1].Clear;
  glMain.GetGauss(0).Draw( Chart1, 1 );
}
end;

procedure TForm1.lbGausesMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  i : Integer;
begin
  edtDx.Value := lbGauses.getSelectedGraph.DX;
  edtDy.Value := Round( lbGauses.GetSelectedGraph.Dy );
  edtScale.Value := lbGauses.GetSelectedGraph.Scale;
  edtFat.Value := lbGauses.GetSelectedGraph.Fat ;
  cbxShowCurGauss.Checked := lbGauses.GetSelectedGraph.Visible;

  Button1.Click;

end;

procedure TForm1.edtDxChange(Sender: TObject);
begin
  if edtDx.Text = '' then exit;
  try
  if lbGauses.GetSelectedGraph.Dx <> edtDx.Value then begin
    lbGauses.GetSelectedGraph.Dx := edtDx.Value;
  end;
  Button1.Click;
  except
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  g : TgGauss;
  series : TLineSeries;
begin
  //series := TLineSeries.Create( nil );
  //series.Title := edtBandCaption.Text;
  //Chart1.SeriesList.Add( series );

  g := CreateGauss;
  g.Fat := 0.1;
  g.Scale := 0;
  g.DX := ( g.maxX + g.minX ) / 2; 
  g.Visible := true;
//  g.Caption :=  edtBandCaption.Text;

  //lbGauses.AddGauss( g );

 // lbGauses.AddItem( 'a', nil );
end;

procedure TForm1.edtScaleChange(Sender: TObject);
begin
  try
  if lbGauses.GetSelectedGraph.Scale <> edtScale.Value then begin
    lbGauses.GetSelectedGraph.Scale := edtScale.Value;
  end;
  Button1.Click;
  except
  end;
end;

procedure TForm1.edtFatChange(Sender: TObject);
begin
  if edtFat.Value = 0 then begin
    edtFat.Value := 0.00000001;
    exit;
  end;
  try
  if lbGauses.GetSelectedGraph.Fat <> edtFat.Value then begin
    lbGauses.GetSelectedGraph.Fat := edtFat.Value;
  end;

  Button1.Click;
  except
  end;
end;

procedure TForm1.edtDyChange(Sender: TObject);
begin
  try
  if lbGauses.GetSelectedGraph.dy <> edtdy.Value then begin
    lbGauses.GetSelectedGraph.dy := edtdy.Value;
  end;

  Button1.Click;
  except
  end;
end;

procedure TForm1.cbxShowCurGaussClick(Sender: TObject);
begin
  try
  if lbGauses.GetSelectedGraph.Visible <> cbxShowCurGauss.Checked then begin
    lbGauses.GetSelectedGraph.visible := cbxShowCurGauss.Checked;
  end;

  Button1.Click;
  except
  end;
end;

procedure TForm1.actOpenExecute(Sender: TObject);
var
  vexcel : Variant;
  x, y : double;
  IsClose : Boolean;
  currow : Integer;
  procedure ReadNextValues;
  begin
    try
      x :=  VExcel.ActiveSheet.Cells.Item[curRow, 1].Value ;
      y :=  VExcel.ActiveSheet.Cells.Item[curRow, 2].Value;
      inc( currow );
      if ( x = 0 ) and ( y = 0 ) then IsClose := true;
    except
      IsClose:= true;
    end;
  end;

begin
  if dlgOpen.Execute then begin


    lbGauses.FillFromExcel( dlgOpen.FileName );

    source := lbGauses.GetGraph( 0 );
    rand := lbGauses.GetGraph( 1 );

    btnSetMain.Click;

    ShowMessage('���������!');
    Button1.Click;
  end;
end;

procedure TForm1.actGausesLoadExecute(Sender: TObject);
begin
  if dlgOpenGauss.Execute then begin
    lbGauses.LoadFromFile( dlgOpenGauss.FileName  );
    source := lbGauses.GetGraph( 0 );
    rand := lbGauses.GetGraph( 1 );
    btnSetMain.Click;
  end;
  Button1.Click;
end;

procedure TForm1.actGausesSaveExecute(Sender: TObject);
begin
  if dlgSaveGauss.Execute then begin
    lbGauses.SaveToFile( dlgSaveGauss.FileName );
  end
end;

procedure TForm1.edtdx_stepChange(Sender: TObject);
begin
  try
    edtDx.Increment := strToFloat(edtdx_step.text );
    edtScale.Increment := strToFloat(edtscale_step.text );
    edtFat.Increment := strToFloat(edtfat_step.text );
  except

  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  gauss := TgGauss( lbGauses.GetSelectedGraph );
  glSubs.AddGraph( gauss );
  Button1.Click;
end;

procedure TForm1.btnSetMainClick(Sender: TObject);
begin
  gauss := TgGauss( lbGauses.GetSelectedGraph );
{  if glMain.Count = 1 then begin
    glMain.DelSelectedGauss;
  end;
}  glMain.AddGraph( gauss );
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  glSubs.DelSelectedGraph;
  Button1.Click;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  gauss := TgGauss(  lbGauses.GetSelectedGraph );
//  if glMain.Count = 1 then begin
    glMain.DelSelectedGraph;
//  end;
//  glMain.AddGauss( gauss );
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
var
  Excel : Variant;
  i, j : Integer;
  gauss : TgGauss;
begin
  Excel := CreateOleObject('Excel.Application');
  Excel.WorkBooks.Add; //������� ����� �������

  for i := 0 to lbGauses.Count - 1 do begin
    gauss := TgGauss( lbGauses.GetGraph( i ) );
    Excel.ActiveSheet.Cells.Item[1, i * 3 + 1].Value := gauss.Caption;
    for j := 0 to gauss.stepCount -1 do begin
      Excel.ActiveSheet.Cells.Item[j + 2, i * 3 + 1].Value := gauss.GetXValueByIndex( j );
      Excel.ActiveSheet.Cells.Item[j + 2, i * 3 + 2].Value := gauss.GetValue( j );
    end;
  end;



  Excel.Visible := True;
  Excel.Range['A1', 'Z1'].Font.FontStyle := 'Bold';
end;

procedure TForm1.BitBtn6Click(Sender: TObject);
var
  vexcel : Variant;
  x, y : double;
  IsClose : Boolean;
  currow : Integer;
  procedure ReadNextValues;
  begin
    try
      x :=  VExcel.ActiveSheet.Cells.Item[curRow, 1].Value ;
      y :=  VExcel.ActiveSheet.Cells.Item[curRow, 2].Value;
      inc( currow );
      if ( x = 0 ) and ( y = 0 ) then IsClose := true;
    except
      IsClose:= true;
    end;
  end;

begin
  if dlgOpen.Execute then begin


    ShowMessage('���������!' + IntToStr( spectrum.FillFromExcel( dlgOpen.FileName ) ) );
  end;

end;

procedure TForm1.BitBtn7Click(Sender: TObject);
begin
  spectrum.DrawToSeries( chart2, 0, true );
end;

end.
