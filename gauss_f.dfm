object Form1: TForm1
  Left = 214
  Top = 113
  Width = 1129
  Height = 594
  Caption = #1043#1072#1091#1089#1089#1080#1072#1085#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1113
    Height = 555
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object Label1: TLabel
        Left = 112
        Top = 416
        Width = 43
        Height = 13
        Caption = #1057#1088#1077#1076#1085#1077#1077
      end
      object Label2: TLabel
        Left = 112
        Top = 440
        Width = 46
        Height = 13
        Caption = #1052#1072#1089#1096#1090#1072#1073
      end
      object Label3: TLabel
        Left = 112
        Top = 464
        Width = 39
        Height = 13
        Caption = #1064#1080#1088#1080#1085#1072
      end
      object Label4: TLabel
        Left = 112
        Top = 488
        Width = 38
        Height = 13
        Caption = #1042#1099#1089#1086#1090#1072
      end
      object Label5: TLabel
        Left = 312
        Top = 392
        Width = 23
        Height = 13
        Caption = #1064#1072#1075':'
      end
      object Button1: TButton
        Left = 552
        Top = 488
        Width = 75
        Height = 25
        Caption = 'Button1'
        TabOrder = 0
        Visible = False
        OnClick = Button1Click
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1105
        Height = 369
        Align = alTop
        TabOrder = 1
        object Splitter1: TSplitter
          Left = 1100
          Top = 15
          Height = 352
          Align = alRight
        end
        object Splitter2: TSplitter
          Left = 912
          Top = 15
          Height = 352
          Align = alRight
        end
        object Chart1: TChart
          Left = 2
          Top = 15
          Width = 910
          Height = 352
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          Title.Text.Strings = (
            #1043#1083#1072#1074#1085#1099#1081)
          Legend.LegendStyle = lsSeries
          View3D = False
          Align = alClient
          TabOrder = 0
          object Series1: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clRed
            Title = 'Main'
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series16: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clTeal
            Title = 'diff'
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series15: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clFuchsia
            Title = 'Series1'
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series2: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGreen
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series3: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clBlack
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series4: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clYellow
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series5: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clBlue
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series6: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clWhite
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series7: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series8: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series9: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series10: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series11: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series12: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series13: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series14: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object Series17: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clNavy
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
        end
        object GroupBox2: TGroupBox
          Left = 915
          Top = 15
          Width = 185
          Height = 352
          Align = alRight
          Caption = #1057#1087#1080#1089#1086#1082' '#1074#1099#1095#1080#1090#1072#1077#1084#1099#1093' '#1075#1088#1072#1092#1080#1082#1086#1074
          TabOrder = 1
          object GroupBox3: TGroupBox
            Left = 2
            Top = 193
            Width = 181
            Height = 157
            Align = alClient
            Caption = #1042#1099#1095#1080#1090#1072#1077#1084#1099#1077
            TabOrder = 0
          end
          object GroupBox4: TGroupBox
            Left = 2
            Top = 15
            Width = 181
            Height = 178
            Align = alTop
            TabOrder = 1
            DesignSize = (
              181
              178)
            object btnSetMain: TButton
              Left = 4
              Top = 16
              Width = 169
              Height = 25
              Anchors = [akLeft, akTop, akRight]
              Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1083#1072#1074#1085#1099#1081
              TabOrder = 0
              OnClick = btnSetMainClick
            end
            object Button5: TButton
              Left = 4
              Top = 48
              Width = 169
              Height = 25
              Anchors = [akLeft, akTop, akRight]
              Caption = #1059#1076#1072#1083#1080#1090#1100' '#1075#1083#1072#1074#1085#1099#1081
              TabOrder = 1
              OnClick = Button5Click
            end
          end
        end
      end
      object Button2: TButton
        Left = 920
        Top = 400
        Width = 153
        Height = 25
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1083#1086#1089#1091
        TabOrder = 2
        OnClick = Button2Click
      end
      object edtDy: TSpinEdit
        Left = 176
        Top = 480
        Width = 121
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 3
        Value = 500
        OnChange = edtDyChange
      end
      object cbxShowCurGauss: TCheckBox
        Left = 112
        Top = 384
        Width = 185
        Height = 17
        Alignment = taLeftJustify
        Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1090#1077#1082#1091#1097#1091#1102' '#1087#1086#1083#1086#1089#1091
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = cbxShowCurGaussClick
      end
      object BitBtn1: TBitBtn
        Left = 8
        Top = 384
        Width = 75
        Height = 25
        Action = actOpen
        Caption = #1054#1090#1082#1088#1099#1090#1100
        TabOrder = 5
      end
      object BitBtn2: TBitBtn
        Left = 920
        Top = 448
        Width = 153
        Height = 25
        Action = actGausesSave
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1087#1088#1086#1077#1082#1090
        TabOrder = 6
      end
      object BitBtn3: TBitBtn
        Left = 920
        Top = 480
        Width = 153
        Height = 25
        Action = actGausesLoad
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1087#1088#1086#1077#1082#1090
        TabOrder = 7
      end
      object edtdx_step: TEdit
        Left = 312
        Top = 408
        Width = 121
        Height = 21
        TabOrder = 8
        Text = '0,01'
        OnChange = edtdx_stepChange
      end
      object edtScale_step: TEdit
        Left = 312
        Top = 432
        Width = 121
        Height = 21
        TabOrder = 9
        Text = '10'
        OnChange = edtdx_stepChange
      end
      object edtFat_step: TEdit
        Left = 312
        Top = 456
        Width = 121
        Height = 21
        TabOrder = 11
        Text = '0,01'
        OnChange = edtdx_stepChange
      end
      object Button3: TButton
        Left = 760
        Top = 376
        Width = 75
        Height = 25
        Caption = '<<'
        TabOrder = 13
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 760
        Top = 408
        Width = 75
        Height = 25
        Caption = '>>'
        TabOrder = 15
        OnClick = Button4Click
      end
      object edtBandCaption: TEdit
        Left = 928
        Top = 376
        Width = 121
        Height = 21
        TabOrder = 10
        Text = #1055#1086#1083#1086#1089#1072
      end
      object BitBtn4: TBitBtn
        Left = 760
        Top = 457
        Width = 153
        Height = 25
        Caption = #1042' '#1101#1082#1089#1077#1083#1100' '#1090#1072#1073#1083#1080#1094#1086#1081
        TabOrder = 12
        OnClick = BitBtn4Click
      end
      object BitBtn5: TBitBtn
        Left = 760
        Top = 481
        Width = 153
        Height = 25
        Caption = #1042' '#1101#1082#1089#1077#1083#1100' '#1087#1072#1088#1072#1084#1077#1090#1088#1072#1084#1080
        TabOrder = 14
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object GroupBox5: TGroupBox
        Left = 0
        Top = 0
        Width = 1105
        Height = 313
        Align = alTop
        Caption = 'GroupBox5'
        TabOrder = 0
        object GroupBox6: TGroupBox
          Left = 918
          Top = 15
          Width = 185
          Height = 296
          Align = alRight
          Caption = 'GroupBox6'
          TabOrder = 0
        end
        object Chart2: TChart
          Left = 2
          Top = 15
          Width = 916
          Height = 296
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          Title.Text.Strings = (
            #1043#1083#1072#1074#1085#1099#1081)
          Legend.LegendStyle = lsSeries
          View3D = False
          Align = alClient
          TabOrder = 1
          object LineSeries1: TPointSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clRed
            Title = 'Main'
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = True
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries2: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clTeal
            Title = 'diff'
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries3: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clFuchsia
            Title = 'Series1'
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries4: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGreen
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries5: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clBlack
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries6: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clYellow
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries7: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clBlue
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries8: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clWhite
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries9: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries10: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries11: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries12: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries13: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries14: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries15: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries16: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clGray
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
          object LineSeries17: TLineSeries
            Marks.ArrowLength = 8
            Marks.Visible = False
            SeriesColor = clNavy
            Pointer.InflateMargins = True
            Pointer.Style = psRectangle
            Pointer.Visible = False
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Y'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
          end
        end
      end
      object BitBtn6: TBitBtn
        Left = 8
        Top = 320
        Width = 75
        Height = 25
        Caption = 'Open'
        TabOrder = 1
        OnClick = BitBtn6Click
      end
      object BitBtn7: TBitBtn
        Left = 224
        Top = 320
        Width = 75
        Height = 25
        Caption = 'BitBtn7'
        TabOrder = 2
        OnClick = BitBtn7Click
      end
    end
  end
  object ActionManager1: TActionManager
    Left = 576
    Top = 488
    StyleName = 'XP Style'
    object actOpen: TAction
      Category = 'File'
      Caption = #1054#1090#1082#1088#1099#1090#1100
      OnExecute = actOpenExecute
    end
    object actGausesSave: TAction
      Category = #1043#1072#1091#1089#1089#1080#1072#1085#1099
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1088#1072#1079#1073#1080#1077#1085#1080#1077
      OnExecute = actGausesSaveExecute
    end
    object actGausesLoad: TAction
      Category = #1043#1072#1091#1089#1089#1080#1072#1085#1099
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1088#1072#1079#1073#1080#1077#1085#1080#1077
      OnExecute = actGausesLoadExecute
    end
  end
  object dlgOpen: TOpenDialog
    Filter = 'Excel worksheet|*.xls;*.xlsx|All|*.*'
    Left = 968
    Top = 120
  end
  object dlgSaveGauss: TSaveDialog
    DefaultExt = '.gf'
    Filter = 'Gauss Files|*.gf'
    Left = 968
    Top = 208
  end
  object dlgOpenGauss: TOpenDialog
    Filter = 'Gauss Files|*.gf'
    Left = 968
    Top = 272
  end
end
