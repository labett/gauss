unit main_f;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ToolWin, ActnMan, ActnCtrls, ActnMenus,
  XPStyleActnCtrls, TeEngine, Series, TeeProcs, Chart, ExtCtrls,
  spectrum_classes, ComObj, StdCtrls, CheckLst, Spin, Buttons, ComCtrls,
  Mask, RXSpin, RxGIF;

type
  TfrmMain = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    chrtMain: TChart;
    ActionManager1: TActionManager;
    ActionMainMenuBar1: TActionMainMenuBar;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    dlgOpenExcel: TOpenDialog;
    glMain: TSGraphList;
    GroupBox1: TGroupBox;
    edtZero: TSpinEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtScaleStep: TEdit;
    BitBtn1: TBitBtn;
    edtRmsStep: TEdit;
    edtPosStep: TEdit;
    edtScale: TRxSpinEdit;
    edtRms: TRxSpinEdit;
    edtPos: TRxSpinEdit;
    BitBtn2: TBitBtn;
    actSaveProject: TAction;
    dlgSaveProj: TSaveDialog;
    dlgOpenProj: TOpenDialog;
    Action4: TAction;
    cbxZero: TCheckBox;
    actToExcel: TAction;
    actCopyToClipboard: TAction;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    lblFullRMSE: TLabel;
    Label5: TLabel;
    lblPrevRMSE: TLabel;
    edtName: TEdit;
    Action5: TAction;
    actImportCsv: TAction;
    Image1: TImage;
    procedure Action2Execute(Sender: TObject);
    procedure Action3Execute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure glMainClick(Sender: TObject);
    procedure edtZeroChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure edtScaleChange(Sender: TObject);
    procedure edtPosChange(Sender: TObject);
    procedure edtScaleStepChange(Sender: TObject);
    procedure edtRmsChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure cbxZeroClick(Sender: TObject);
    procedure actSaveProjectExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure actToExcelExecute(Sender: TObject);
    procedure actCopyToClipboardExecute(Sender: TObject);
    procedure edtNameChange(Sender: TObject);
    procedure Action5Execute(Sender: TObject);
    procedure actImportCsvExecute(Sender: TObject);
  private
    spectrum : TSGraph;
  public
    procedure RepaintRMSE;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

procedure TfrmMain.Action2Execute(Sender: TObject);
begin
  close;
end;

procedure TfrmMain.Action3Execute(Sender: TObject);
var
  vexcel : Variant;
  x, y : double;
  IsClose : Boolean;
  currow : Integer;
  procedure ReadNextValues;
  begin
    try
      x :=  VExcel.ActiveSheet.Cells.Item[curRow, 1].Value ;
      y :=  VExcel.ActiveSheet.Cells.Item[curRow, 2].Value;
      inc( currow );
      if ( x = 0 ) and ( y = 0 ) then IsClose := true;
    except
      IsClose:= true;
    end;
  end;

begin
  dlgOpenExcel.Filter := 'Excel|*.xls';
  if dlgOpenExcel.Execute then begin
    ShowMessage('���������!' + IntToStr( spectrum.FillFromExcel( dlgOpenExcel.FileName ) ) );
  end;
  spectrum.Caption := 'Main spectrum';

  glMain.AddMain( spectrum );
end;


procedure TfrmMain.FormCreate(Sender: TObject);
begin
  spectrum := TSGraph.Create;
  frmMain.Caption := 'Gauss v2.03'
end;

procedure TfrmMain.glMainClick(Sender: TObject);
begin
  edtScale.Text := FloatToStr( glMain.Scale );
  edtRms.Text := FloatToStr( glMain.RmsWidth );
  edtPos.Text := FloatToStr( glMain.Position );

  glMain.DrawGraphs;
end;

procedure TfrmMain.edtZeroChange(Sender: TObject);
begin
  glMain.ZeroLevel := edtZero.Value;
  RepaintRMSE;
end;

procedure TfrmMain.BitBtn1Click(Sender: TObject);
var
  g : TSGauss;
begin
  g := TSGauss.Create;
  g.CopyFrom( glMain.getMain );
  g.Fat := StrToFloat( edtRms.Text );
  g.Scale := StrToFloat( edtScale.Text );
  g.Position := StrToFloat( edtPos.Text );

  glMain.AddGauss( g );
end;

procedure TfrmMain.edtScaleChange(Sender: TObject);
begin
  glMain.Scale := edtScale.Value;
  RepaintRMSE;
end;

procedure TfrmMain.edtPosChange(Sender: TObject);
begin
  glMain.Position := edtPos.Value;
  RepaintRMSE;
end;

procedure TfrmMain.edtScaleStepChange(Sender: TObject);
begin
  edtScale.Increment := StrToFloat( edtScaleStep.Text );
  edtRms.Increment := StrToFloat( edtRmsStep.Text );
  edtPos.Increment := StrToFloat( edtPosStep.Text );
end;

procedure TfrmMain.edtRmsChange(Sender: TObject);
begin
  glMain.RmsWidth := edtRms.Value;
  RepaintRMSE;
end;

procedure TfrmMain.BitBtn2Click(Sender: TObject);
var
  g : TSGauss;
begin
  g := glMain.curGauss;
  if g = nil then exit;
  if( mrOk <> MessageDlg('This spectral line will be deleted: ' + g.Caption, mtConfirmation, [mbOK, mbCancel], 0)  ) then exit;

  glMain.DeleteGauss;
  glMain.DrawGraphs;
end;

procedure TfrmMain.cbxZeroClick(Sender: TObject);
begin
  if cbxZero.Checked then begin
    chrtMain.BottomAxis.AutomaticMinimum := false;
    chrtMain.BottomAxis.Minimum := 0;
  end else chrtMain.BottomAxis.AutomaticMinimum := true;
end;

procedure TfrmMain.actSaveProjectExecute(Sender: TObject);
begin
  if dlgSaveProj.Execute then begin
    glMain.SaveToIni( dlgSaveProj.FileName );
  end;
end;

procedure TfrmMain.Action1Execute(Sender: TObject);
begin
  if dlgOpenProj.Execute then begin
    glMain.LoadFromIni( dlgOpenProj.FileName );
  end;
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := false;
  if (not glMain.IsSaved ) then begin
    if( mrYes <> MessageDlg(
      'Project not saved. Do you want to exit? ( Press <Yes> if you want to loose all changes )'
      , mtConfirmation, [mbYes, mbCancel], 0)
    ) then begin
      glMain.SaveToIni('G2_backup.g2p');
      CanClose := true;
      exit;
    end;

  end;
  CanClose := true;
end;

procedure TfrmMain.actToExcelExecute(Sender: TObject);
begin
  glMain.ExportToExcel;
end;

procedure TfrmMain.actCopyToClipboardExecute(Sender: TObject);
begin
  chrtMain.CopyToClipboardBitmap;
end;

procedure TfrmMain.RepaintRMSE;
begin
  lblFullRMSE.Caption := FloatToStr( glMain.RMSE );
  lblPrevRMSE.Caption := FloatToStr( glMain.RMSEPrev );

  lblFullRMSE.Font.Color := clBlack;
  if( glMain.RMSE < glMain.RMSEPrev ) then lblFullRMSE.Font.Color := clGreen;
  if( glMain.RMSE > glMain.RMSEPrev ) then lblFullRMSE.Font.Color := clRed;
end;

procedure TfrmMain.edtNameChange(Sender: TObject);
begin
  glMain.getMain.Caption := edtName.Text;
  glMain.DrawGraphs;
end;

procedure TfrmMain.Action5Execute(Sender: TObject);
begin
  ShowMessage('��� ������ ��������� ������ ����� File->Import Excel. � excel ����� ������ ���� ��� ��������, X � Y, ��� ������� ���� � ��� ���������. '+
               '����� �������� ������ �������� ����� ��� ������� -- ��������, ������� ����� �������� � ������������� ��������, � �������� (�� ���� = 0)'+
               #10 + #13 + '  ����� �������� ������, ������� ������ AddNewGauss.' +
               #10 + #13 + ' ����� ����� ��������� ������' +
               #10 + #13 + ' ��� �������� ���� ������ File->Export, � �������� ��� � ������'+
               #10 + #13 + ' Import CSV ������ ��� �������� �����, ������������ ��������. ����������� ���������� ����� -- �����. ������ ������ ������������(��������� ���������)');
end;

procedure TfrmMain.actImportCsvExecute(Sender: TObject);
begin
  dlgOpenExcel.Filter := 'CSV|*.*';
  if dlgOpenExcel.Execute then begin
    ShowMessage('���������!' + IntToStr( spectrum.FillFromCSV( dlgOpenExcel.FileName ) ) );
  end;
  spectrum.Caption := 'Main spectrum';

  glMain.AddMain( spectrum );
end;

end.
